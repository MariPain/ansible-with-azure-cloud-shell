-----

que FALTA AQUI?:

ACCEDER A LA MAQUINA PARA DEPLOY
- BASTION FOR CONNECTION?

## PLAYBOOK FOR DEPLOY
- añadir tarea para generacion de claves ssh
- VIRTUAL MACHINE ACCES: Una vez creada la maquina virtual es necesario acceder a ella bien por ssh o bien por Bastion (preferible)
- VIRTUAL MACHINE UPDATE: Tareas de bash apt-get update
- DOCKER INTSALL: descarga e intalacion de docker. 

LOS SIGUIENTES PASOS SON CONECTARME A LA MAQUINA, ACTUALIZAR REPOSITORIOS, DESCARGAR DOKER, CONVERTIR EN TAREAS DOCKERFILE.ODDOO DOCKERFILE.POSTGRES Y DOCKER-COMPOSE PARA EL DEPLOY


- ME PREGUNTO, SI USO BASTION, COMO ACCEDO A ODOO? MEDIANTE BASTION O MEDIANTE LA IP PUBLICA DE ODOO SERVICE (MI VM)?

EN CUANTO AL INVENTORY EN ESTE CONTEXTO, ES NECESARIO?
COMO SE VERIA?

ESTOS SON LOS ARCHIVOS DE DEPLOY PARA DOCKER:

(copio y pego de repo anterior)

RQUITECTURA DOCKER
CREACION SIGUIENDO LOS PRINCIPIOS DE DOCKER DE CONTENEDOR POR SERVICIO


ODOO: que ofrece? que es? que necesita para funcionar?


DATABASE (postgres)



instalacion de DOCKER

Ir a documentacion oficial Doker y buscar la guia para ubuntu, seguirla y listo. hacerlo desde mobaxterm.
INSTALLATION:
The Docker installation package available in the official Ubuntu repository may not be the latest version. To ensure we get the latest version, we’ll install Docker from the official Docker repository. To do that, we’ll add a new package source, add the GPG key from Docker to ensure the downloads are valid, and then install the package.
Étape 1 : préparation du système¶
En premier lieu, assurez-vous de bien disposer de la dernière mise à jour du système et de tous les paquets. Pour cela, saisissez les commandes suivantes dans le terminal afin de procéder à la mise à jour du système :

# Add Docker's official GPG key:
sudo apt-get update
sudo apt-get install ca-certificates curl
sudo install -m 0755 -d /etc/apt/keyrings
sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc
sudo chmod a+r /etc/apt/keyrings/docker.asc

# Add the repository to Apt sources:
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update


. . .

IMAGENES NECESARIAS

sudo docker pull odoo
sudo docker pull postgres

mariola@odoo-docker:~$ sudo docker images
REPOSITORY    TAG       IMAGE ID       CREATED         SIZE
odoo          latest    75a117dd3eff   7 days ago      1.83GB
postgres      latest    b9390dd1ea18   4 weeks ago     431MB
hello-world   latest    d2c94e258dcb   10 months ago   13.3kB
mariola@odoo-docker:~$


nota: Docker es ligero, no almacena cache. SImpre habrqa que actualizar repositorios de los contenedores antes de dscargar lo que sea
lista de comandos:
V(oy a hacer primero una instalacion basica de todo, luego ire haciendo la instalaciony la arquitectura mas eficiente.)

imagenes de CONTENEDOR
configuracion de imagenes


script de dockerfile y docker compose


Dockerfile.odoo

# Utiliza la imagen base de Odoo
FROM odoo
# Expone el puerto 8069
EXPOSE 8069
# Configura el inicio de Odoo
CMD ["odoo"]


Dockerfile.postgres

# Utiliza ultima base de PostgreSQL 
FROM postgres:latest
# Variables de entorno para el usuario, contraseña y base de datos
ENV POSTGRES_USER odoo
ENV POSTGRES_PASSWORD odoo
ENV POSTGRES_DB postgres


docker-compose.yml

version: '3'
services:
  db:
    build:
      context: .
      dockerfile: Dockerfile.postgres
    environment:
      POSTGRES_USER: odoo
      POSTGRES_PASSWORD: odoo
      POSTGRES_DB: postgres
    ports:
      - "5432:5432"
    volumes:
      - postgres_data:/var/lib/postgresql/data
  odoo:
    build:
      context: .
      dockerfile: Dockerfile.odoo
    ports:
      - "80:8069"
    depends_on:
      - db
volumes:
  postgres_data:




Provisional playbook:

Claro, aquí te guiaré en cómo organizar tus tareas y playbook para el despliegue de tus aplicaciones utilizando Docker en tu máquina virtual Azure. Primero, definiremos las tareas que necesitamos realizar y luego las organizaremos en un playbook.

### Tareas a realizar:

1. **Instalación de Docker**: Seguir los pasos de instalación de Docker en tu máquina virtual.
2. **Descargar imágenes de Docker**: Descargar las imágenes de Odoo y PostgreSQL.
3. **Creación de Dockerfiles**: Crear los archivos Dockerfile para Odoo y PostgreSQL.
4. **Creación de docker-compose.yml**: Crear el archivo docker-compose.yml para configurar los contenedores de Odoo y PostgreSQL.
5. **Ejecutar contenedores**: Utilizar docker-compose para ejecutar los contenedores de Odoo y PostgreSQL.
6. **Acceso a la máquina virtual**: Configurar el acceso a la máquina virtual para poder ejecutar las tareas anteriores.

### Organización del playbook:

```yaml
---
- name: Despliegue de aplicaciones con Docker en máquina virtual Azure
  hosts: odoo-service
  remote_user: mariola
  become: yes
  vars:
    dockerfiles_path: "/path/to/dockerfiles"
    ssh_private_key_path: "/path/to/private_key.pem"

  tasks:
    - name: Instalación de Docker
      # Aquí colocarías la tarea para instalar Docker en la máquina virtual

    - name: Descargar imágenes de Docker
      # Aquí colocarías las tareas para descargar las imágenes de Odoo y PostgreSQL

    - name: Copiar Dockerfiles
      copy:
        src: "{{ item }}"
        dest: "{{ dockerfiles_path }}"
      with_items:
        - "Dockerfile.odoo"
        - "Dockerfile.postgres"
      # Aquí copiarías los archivos Dockerfile a la máquina virtual

    - name: Crear docker-compose.yml
      template:
        src: docker-compose.yml.j2
        dest: "{{ dockerfiles_path }}/docker-compose.yml"
      # Aquí usarías un template para crear el docker-compose.yml con las variables necesarias

    - name: Ejecutar contenedores con docker-compose
      shell: docker-compose up -d
      args:
        chdir: "{{ dockerfiles_path }}"
      # Aquí ejecutarías los contenedores de Odoo y PostgreSQL utilizando docker-compose

    - name: Acceso a la máquina virtual
      ansible.builtin.shell: |
        ssh -i {{ ssh_private_key_path }} mariola@{{ ansible_host }}
      # Aquí configurarías el acceso a la máquina virtual mediante SSH usando tu clave privada

```

### Explicación:

- El playbook comienza instalando Docker en la máquina virtual, luego descarga las imágenes de Docker necesarias.
- Luego copia los archivos Dockerfile necesarios a la máquina virtual y crea el archivo docker-compose.yml utilizando un template.
- Después, ejecuta los contenedores de Odoo y PostgreSQL usando docker-compose.
- Finalmente, configura el acceso a la máquina virtual mediante SSH utilizando una clave privada.

Recuerda reemplazar `/path/to/dockerfiles` y `/path/to/private_key.pem` con las rutas correctas en tu entorno. Además, asegúrate de tener los archivos Dockerfile y docker-compose.yml en el directorio correcto en tu máquina local.