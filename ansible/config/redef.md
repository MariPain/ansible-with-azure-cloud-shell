Claro, vamos a crear la estructura necesaria para los roles, las variables y el playbook principal.

1. **Roles**:
   - Crearemos los siguientes roles:
     - `azure_infrastructure`: Para crear la infraestructura en Azure.
     - `configure_security`: Para configurar las reglas de seguridad.
     - `install_docker`: Para instalar Docker en la máquina virtual.
     - `deploy_odoo`: Para desplegar la aplicación Odoo.

2. **Variables**:
   - Crearemos archivos YAML para definir nuestras variables en cada uno de los roles. Por ejemplo:
     - `roles/azure_infrastructure/vars/main.yml`
     - `roles/configure_security/vars/main.yml`
     - `roles/install_docker/vars/main.yml`
     - `roles/deploy_odoo/vars/main.yml`

3. **Playbook principal**:
   - Crearemos un playbook principal que orqueste la ejecución de los roles. Por ejemplo:
     - `main_playbook.yml`

### 1. Roles:

#### a. azure_infrastructure:
   - Crearemos las tareas para crear la infraestructura en Azure, como la VNet, NSG, NIC, IP pública y la máquina virtual.

#### b. configure_security:
   - Configuraremos las reglas de seguridad, permitiendo el tráfico SSH y HTTP.

#### c. install_docker:
   - Instalaremos Docker en la máquina virtual.

#### d. deploy_odoo:
   - Desplegaremos la aplicación Odoo en contenedores Docker.

### 2. Variables:

#### a. vars/main.yml en cada rol:
   - Definiremos las variables necesarias para cada rol. Por ejemplo, las credenciales de Azure, nombres de recursos, ubicaciones, etc.

### 3. Playbook principal:

#### main_playbook.yml:
   - Este playbook será el punto de entrada para la ejecución de los roles. Llamará a los roles en el orden necesario para desplegar la infraestructura y la aplicación Odoo.

### Estructura final del proyecto:

```
ansible_project/
├── roles/
│   ├── azure_infrastructure/
│   │   ├── tasks/
│   │   │   └── main.yml
│   │   └── vars/
│   │       └── main.yml
│   ├── configure_security/
│   │   ├── tasks/
│   │   │   └── main.yml
│   │   └── vars/
│   │       └── main.yml
│   ├── install_docker/
│   │   ├── tasks/
│   │   │   └── main.yml
│   │   └── vars/
│   │       └── main.yml
│   └── deploy_odoo/
│       ├── tasks/
│       │   └── main.yml
│       └── vars/
│           └── main.yml
├── main_playbook.yml
└── inventories/
    └── azure_inventory.yml
```

En cada archivo `main.yml` dentro de los directorios `tasks` de los roles, colocarás las tareas específicas que corresponden a ese rol. En los archivos `main.yml` dentro de los directorios `vars`, definirás las variables necesarias para el rol.

En el playbook `main_playbook.yml`, llamarás a los roles en el orden necesario para desplegar tu infraestructura y la aplicación Odoo.


`azure_infrastructure`:

### main_playbook.yml:
```yaml
---
- name: Deploy Azure Infrastructure and Odoo Application
  hosts: localhost
  connection: local
  gather_facts: no
  become: yes

  tasks:
    - name: Include azure_infrastructure role
      include_role:
        name: azure_infrastructure

    - name: Include configure_security role
      include_role:
        name: configure_security

    - name: Include install_docker role
      include_role:
        name: install_docker

    - name: Include deploy_odoo role
      include_role:
        name: deploy_odoo
```

### roles/azure_infrastructure/tasks/main.yml:
```yaml
---
- name: Create Azure Infrastructure
  azure.azcollection.azure_rm_virtualnetwork:
    resource_group: "{{ resource_group_name }}"
    name: "{{ vnet_name }}"
    address_prefixes: "{{ vnet_address_prefix }}"

- name: Create Subnets
  azure.azcollection.azure_rm_subnet:
    resource_group: "{{ resource_group_name }}"
    virtual_network_name: "{{ vnet_name }}"
    name: "{{ item.name }}"
    address_prefix: "{{ item.prefix }}"
  loop:
    - { name: "{{ subnet_front }}", prefix: "{{ subnet_front_prefix }}" }
    - { name: "{{ subnet_data }}", prefix: "{{ subnet_data_prefix }}" }

- name: Create Public IP
  azure.azcollection.azure_rm_publicipaddress:
    resource_group: "{{ resource_group_name }}"
    allocation_method: Static
    name: "{{ public_ip_address }}"

- name: Create Network Interface with Public IP
  azure.azcollection.azure_rm_networkinterface:
    resource_group: "{{ resource_group_name }}"
    name: "{{ vm_name }}-nic"
    virtual_network_name: "{{ vnet_name }}"
    subnet: "{{ subnet_front }}"
    security_group: "{{ nsg_name }}"
    public_ip_name: "{{ public_ip_address }}"

- name: Create Virtual Machine in Azure with NIC and Managed Disk
  azure.azcollection.azure_rm_virtualmachine:
    resource_group: "{{ resource_group_name }}"
    name: "{{ vm_name }}"
    vm_size: "{{ vm_size }}"
    location: "{{ location }}"
    admin_username: "{{ admin_username }}"
    ssh_password_enabled: false
    ssh_public_keys:
      - path: "/home/{{ admin_username }}/.ssh/authorized_keys"
        key_data: "{{ ssh_public_key }}"
    network_interface_names: "{{ vm_name }}-nic"
    image:
      publisher: "{{ vm_publisher }}"
      offer: "{{ vm_offer }}"
      sku: "{{ vm_sku }}"
      version: "{{ vm_version }}"
    managed_disk_type: "Standard_LRS"
    state: present
```

